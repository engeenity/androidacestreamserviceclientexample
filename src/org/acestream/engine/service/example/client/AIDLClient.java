package org.acestream.engine.service.example.client;

import org.acestream.engine.service.v0.AceStreamEngineMessages;
import org.acestream.engine.service.v0.IAceStreamEngine;
import org.acestream.engine.service.v0.IAceStreamEngineCallback;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.widget.Toast;

public class AIDLClient extends BaseClient {

	private IAceStreamEngine mService = null;
	private boolean mBound = false;
	
	private IAceStreamEngineCallback mCallback = new IAceStreamEngineCallback.Stub() {
		@Override
		public void onUnpacking() throws RemoteException {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_UNPACKING);
			handler.sendMessage(message);
		}
		@Override
		public void onStarting() throws RemoteException {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_STARTING);
			handler.sendMessage(message);
		}
		@Override
		public void onReady(int listenPort) throws RemoteException {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_READY, listenPort, 0);
			handler.sendMessage(message);
		}
		@Override
		public void onWaitForNetworkConnection() throws RemoteException {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_WAIT_CONNECTION);
			handler.sendMessage(message);
		}
		@Override
		public void onStopped() throws RemoteException {
			Handler handler = getHandler();
			Message message = handler.obtainMessage(AceStreamEngineMessages.MSG_ENGINE_STOPPED);
			handler.sendMessage(message);
		}
	};
	
	private ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceDisconnected(ComponentName name) {
			mService = null;
			mBound = false;
		}
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mService = IAceStreamEngine.Stub.asInterface(service);
			try {
				mService.registerCallback(mCallback);
				mService.startEngine();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	};
	
	public AIDLClient(Context context) {
		super(context);
	}

	@Override
	public void bind() {
		if(!mBound) {
			Intent intent = new Intent(org.acestream.engine.service.v0.IAceStreamEngine.class.getName());
			mBound = getContext().bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
			if(!mBound) {
				Toast.makeText(getContext(), "Failed to bind", Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	public void unbind() {
		if(mBound) {
			if(mService != null) {
				try {
					mService.unregisterCallback(mCallback);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			getContext().unbindService(mConnection);
			mBound = false;
		}
	}
}
